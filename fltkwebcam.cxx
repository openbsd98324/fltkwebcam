
//////////////////////////////////////////
//////////////////////////////////////////
// first one draft
// https://termbin.com/kxqv
// Compile me with :    c++  -I/usr/X11R7/include/ -I/usr/pkg/include   -L/usr/pkg/lib  -lfltk  -lfltk_images  source/fltk/fltkwebcam.cxx                 -o   source/bin/fltkwebcam 
//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
#include <stdio.h>
#if defined(__linux__) //linux
#define MYOS 1
#elif defined(_WIN32)
#define MYOS 2
#elif defined(_WIN64)
#define MYOS 3
#elif defined(__unix__) 
#define MYOS 4  // freebsd
#define PATH_MAX 2500
#else
#define MYOS 0
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>  
//////////////////////////////////////////
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_PNG_Image.H>
//////////////////////////////////////////


char fileread[PATH_MAX];
char fileimg[PATH_MAX];
int camslot = 1;

// Global pointers for the GUI objects
Fl_Window *mywindow;
Fl_Button *lbutton;
Fl_Box *mypicturebox;


int fexist(const char *a_option)
{
  char dir1[PATH_MAX]; 
  char *dir2;
  DIR *dip;
  strncpy( dir1 , "",  PATH_MAX  );
  strncpy( dir1 , a_option,  PATH_MAX  );

  struct stat st_buf; 
  int status; 
  int fileordir = 0 ; 

  status = stat ( dir1 , &st_buf);
  if (status != 0) {
    fileordir = 0;
  }
  FILE *fp2check = fopen( dir1  ,"r");
  if( fp2check ) {
  fileordir = 1; 
  fclose(fp2check);
  } 

  if (S_ISDIR (st_buf.st_mode)) {
    fileordir = 2; 
  }
  return fileordir;
/////////////////////////////
}





/////////////////////////////
// FLTK starts...
/////////////////////////////
int modefileread = 1;
void cb_Update(void*) 
{
    printf( "Time\n" );
    strncpy( fileimg, fileread , PATH_MAX );

    if ( camslot == 2 ) 
       system( " rm webcam.png ; wget \"http://meteo.arso.gov.si/uploads/probase/www/observ/webcam/LJUBL-ANA_BEZIGRAD_dir/siwc_LJUBL-ANA_BEZIGRAD_s-zoom1_latest.jpg\"     -O  webcam.jpg ; convert webcam.jpg webcam.png " );

    else if ( camslot == 1 ) 
       system( " rm webcam.png ; wget  \"http://meteo.arso.gov.si/uploads/probase/www/observ/webcam/KOPER_MARKOVEC_dir/siwc_KOPER_MARKOVEC_n_latest.jpg\"            -O  webcam.jpg ; convert webcam.jpg webcam.png  ");
    else 
    system( " rm webcam.png ; wget  \"http://meteo.arso.gov.si/uploads/probase/www/observ/webcam/KOPER_MARKOVEC_dir/siwc_KOPER_MARKOVEC_n_latest.jpg\"            -O  webcam.jpg ; convert webcam.jpg webcam.png  ");

    Fl_PNG_Image *limg;
    limg = new Fl_PNG_Image( fileimg );
    mypicturebox->image(limg);
    mypicturebox->redraw();
    mywindow->redraw();
    Fl::repeat_timeout( 5.00, cb_Update);
}




//////////////////////////////////////////
//////////////////////////////////////////
int main( int argc, char *argv[])
{

   if ( argc == 2 )
   if ( strcmp( argv[ 1 ] , "--help" ) == 0 ) 
   {
        printf( "Freedom is power:   [ fltkwebcam  1  ]   or [ fltkwebcam 2 ] \n" );
        printf( " Compile me with :    c++  -I/usr/X11R7/include/ -I/usr/pkg/include   -L/usr/pkg/lib  -lfltk  -lfltk_images  source/fltk/fltkwebcam.cxx                 -o   source/bin/fltkwebcam " );
	printf( "bye\n" );
        return 0;
   }


   strncpy( fileread, "webcam.png" , PATH_MAX );
   if ( argc == 2 )
   {
       if ( strcmp( argv[ 1 ] , "1" ) == 0 ) 
        camslot = 1 ;
       else if ( strcmp( argv[ 1 ] , "2" ) == 0 ) 
        camslot = 2 ;
   }


    mywindow = new Fl_Window( 800, 600, "FLTK image demo");
    mywindow->resizable(mywindow);

    mypicturebox = new Fl_Box(10, 10, 700, 500);
    mywindow->end();
    mywindow->show();

    Fl::add_timeout(2.0, cb_Update);
    return(Fl::run());
}




